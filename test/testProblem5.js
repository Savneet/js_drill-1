const inventory = require("../carData.js");
const CarYearsArrayProblem = require("../problem4.js");
const listOlderCarProblem = require("../problem5.js");

let CarYearsArray = CarYearsArrayProblem(inventory);
try {
  console.log(listOlderCarProblem(inventory, CarYearsArray));
  console.log(listOlderCarProblem(inventory, CarYearsArray).length +" cars are older than 2000");
} catch (error) {
  console.log("There is some error in your code");
}
