const inventory = require("./carData.js");

// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModel(inventory) {
  if (Array.isArray(inventory)) {
    let inventorySize = inventory.length;
    //Using Bubble Sort
    for (let index1 = 0; index1 < inventorySize; index1++ ){
      for (let index2 = 0; index2 < inventorySize - index1 - 1; index2++) {
        if (inventory[index2].car_model > inventory[index2 + 1].car_model) {
          let temp = inventory[index2];
          inventory[index2] = inventory[index2 + 1];
          inventory[index2 + 1] = temp;
        }
      }
    }
    for (let carInfo of inventory) {
      console.log(carInfo.car_model);
    }
  } else {
    return null;
  }
}

module.exports = sortCarModel;
