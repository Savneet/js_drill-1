const inventory = require("./carData.js");

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function listCarYears(inventory) {
  if (Array.isArray(inventory)) {
    let carYearArray=[];
    for(carInfo of inventory)
    {
        carYearArray.push(carInfo.car_year);
    }
    return carYearArray;
  } else {
    return null;
  }
}

module.exports = listCarYears;
