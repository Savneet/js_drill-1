const inventory = require("./carData.js");

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function listBmwAndAudiCars(inventory) {
  if (Array.isArray(inventory)) {
    let BmwAndAudiCarArray=[];
    for(carInfo of inventory)
    {
        if(carInfo.car_make=="BMW" || carInfo.car_make=="Audi" )
        {
            BmwAndAudiCarArray.push(carInfo);
        }
    }
    return BmwAndAudiCarArray;
  } else {
    return null;
  }
}

module.exports = listBmwAndAudiCars;
