const inventory = require("./carData.js");

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function findCarWithId(inventory, id) {
  if (Array.isArray(inventory) && typeof id === "number") {
    for (let carInfo of inventory) {
      if (carInfo.id === id) {
        return `Car ${id} is a ${carInfo.car_year} ${carInfo.car_make} ${carInfo.car_model}`;
      }
    }
  }
  else{
    return null;
  }
}
//console.log(findCarWithId(inventory, 33));
module.exports = findCarWithId;

